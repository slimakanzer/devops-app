FROM eclipse-temurin:17-jdk-alpine
WORKDIR /workspace/app

COPY gradlew .
COPY build.gradle .
COPY settings.gradle .
COPY gradle gradle
COPY src src

EXPOSE 8080

RUN chmod +x gradlew
RUN ./gradlew build

CMD ["./gradlew", "bootRun"]
