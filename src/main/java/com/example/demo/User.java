package com.example.demo;

import org.hibernate.validator.constraints.Range;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="users")
public class User {
    
    @Id
    @Range(min = 100000, max = 999999)
    private long id;

    @Column(length = 128, nullable = false)
    private String name;
}
